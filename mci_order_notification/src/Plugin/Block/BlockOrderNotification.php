<?php

namespace Drupal\mci_order_notification\Plugin\Block;

use Drupal\commerce_order\Entity\OrderItem;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;

/**
 * Provides a 'BlockOrderNotification' block.
 *
 * @Block(
 *  id = "block_order_notification",
 *  admin_label = @Translation("Block order notification"),
 * )
 */
class BlockOrderNotification extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['#cache']['max-age'] = 0;
    $res = [];
    $store = \Drupal::service('commerce_store.current_store')->getStore();
    $cart = \Drupal::service('commerce_cart.cart_provider')
      ->getCart('default', $store);
    if ($cart) {
      $cart = $cart->toArray();
      $ids = $cart['order_items'];
      $res = $this->_smart_notification_logic($ids);
      // warning when cart is empty
      if (!empty($res)) {
        $build['#attached']['library'][] = 'mci_order_notification/smart-js';
        $build['#theme'] = 'block__smart';
        $build['#results'] = ($res) ? $res : '';
      }
    }
    return $build;
  }

  public function _smart_notification_logic($ids) {
    $res = [];
    foreach ($ids as $key => $id) {
      $order_item = OrderItem::load($id['target_id']);
      $item = ($order_item) ? $order_item->getPurchasedEntity() : FALSE;
      if (!empty($item)) {
        $it = $item->toArray();
        $variation_id = $it['variation_id'][0]['value'];
        $product_id = $it['product_id'][0]['target_id'];
        $title = $item->label();
        $image = $it['field_media_product_image'][0]['target_id'];
        $database = \Drupal::database();
        $prod_image = $database->select('media__field_product_image', 'img')
          ->fields('img', ['field_product_image_target_id'])
          ->condition('entity_id', $image, '=')
          ->execute()
          ->fetchCol();
        $file = File::load($prod_image[0]);
        $url = ImageStyle::load('thumbnail')
          ->buildUrl($file->getFileUri());
        $result = $database->select('commerce_stock_transaction', 'st')
          ->fields('st', ['qty'])
          ->condition('entity_id', $variation_id, '=')
          ->execute()
          ->fetchCol();
        $stock = array_sum($result);
        if ($stock && $stock < 10) {
          $prod_url = Url::fromUri($GLOBALS["base_url"] . '/' . $product_id . '?v=' . $variation_id);
          $link_options = array(
            'attributes' => array(
              'class' => array(
                'smart-product-link',
              ),
            ),
          );
          $prod_url->setOptions($link_options);
          $res[$key] = [
            'item' => [
              'title' => $title,
              'stock' => $stock,
              'image' => $url,
              'link' => $prod_url,
            ],
          ];
        }
      }
    }
    return $res;
  }
}
