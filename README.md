# Commerce Smart Notification

Custom extension for drupal8 commerce module.

Smart notification is custom module that will start notice you about a product stock level for products that are in your order list.

Custom block with modal popup that contains information about products stock level that are in your order list.
